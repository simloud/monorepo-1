import React, { useState } from 'react'
import { Row, Col, Input, Button, Tooltip } from 'antd'
import { SettingOutlined } from '@ant-design/icons'
import './App.css'

const { TextArea } = Input

const defaultHost = window.location.host

const App = () => {
  const [host, setHost] = useState(defaultHost ?? '')
  const [responseText, setResponseText] = useState('')
  const [urlFieldVisible, setUrlFieldVisible] = useState(false)

  const requestData = url => {
    fetch(url)
      .then(r => r.json())
      .then(r => setResponseText(JSON.stringify(r)))
  }
  return (
    <Row>
      <Col xs={20} sm={20} md={12} lg={8} xl={8} offset={2}>
        <img src="/logo.svg" alt="Logo" width="200" className="mt-5" />
        <h1 style={{ marginTop: 40 }}>Welcome to Simloud World</h1>

        <h3>Choose the technology that interests you:</h3>
        <Row justify="space-between">
          <Col span={11} className="text-center">
            <Button
              type="primary"
              className="w-full mb-5"
              onClick={() => requestData(`https://k8s.${host}/kube-service-1/`)}
              style={{ height: 47 }}
            >
              <img
                src={`/icons/kubernetes.svg`}
                alt="logo"
                height="35"
                className="mr-2"
              />{' '}
              Kubernetes A
            </Button>
            <Button
              type="primary"
              className="w-full mb-5"
              onClick={() => requestData(`https://lambda.${host}/lambda-service-1/`)}
              style={{ height: 47 }}
            >
              <img src={`/icons/lambda.svg`} alt="logo" height="35" className="mr-2" />{' '}
              Lambda A
            </Button>
            <a
              href={`https://jenkins.${host}/`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <Button type="primary" className="w-full mb-5" style={{ height: 47 }}>
                <img src={`/icons/jenkins.svg`} alt="logo" height="35" className="mr-2" />{' '}
                Jenkins
              </Button>
            </a>
          </Col>
          <Col span={11} className="text-center">
            <Button
              type="primary"
              className="w-full mb-5"
              onClick={() => requestData(`https://k8s.${host}/kube-service-2/`)}
              style={{ height: 47 }}
            >
              <img
                src={`/icons/kubernetes.svg`}
                alt="logo"
                height="35"
                className="mr-2"
              />{' '}
              Kubernetes B
            </Button>
            <Button
              type="primary"
              className="w-full mb-5"
              onClick={() => requestData(`https://lambda.${host}/lambda-service-2/`)}
              style={{ height: 47 }}
            >
              <img src={`/icons/lambda.svg`} alt="logo" height="35" className="mr-2" />{' '}
              Lambda B
            </Button>
            <Row>
              <Col span={4}>
                <Tooltip title="sub domain">
                  <SettingOutlined
                    className="mt-4"
                    onClick={() => setUrlFieldVisible(!urlFieldVisible)}
                  />
                </Tooltip>
              </Col>
              <Col span={20}>
                <Input
                  placeholder="sub domain"
                  value={host}
                  className={`mt-2 ${urlFieldVisible ? 'block' : 'hidden'}`}
                  onChange={({ target: { value } }) => setHost(value)}
                />
              </Col>
            </Row>
          </Col>
        </Row>

        <TextArea
          placeholder="Response goes here..."
          rows="7"
          className="mt-5"
          value={responseText}
        />
      </Col>
      <Col
        xs={16}
        sm={16}
        md={6}
        lg={10}
        xl={10}
        offset={1}
        className="flex items-center"
      >
        <iframe
          src="https://topology.netlify.app/?client=canaveral"
          title="Topology"
          width={528}
          height={396}
          frameBorder="0"
          style={{ maxWidth: '100%' }}
        />
      </Col>
    </Row>
  )
}

export default App
